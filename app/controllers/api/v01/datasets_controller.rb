class Api::V01::DatasetsController < ApplicationController
	include Restrictions
  include Tools

  before_action :dataset_name, except: [:update]

  def all
		params.permit!
  	if params.to_h.size <= 3
  		resultado = @dataset.all
  		json_response resultado
  	else
  		consulta()
  	end
  end

  def consulta
    campos = ignored_params params
  	if campos.has_key? 'q'
  		resultados = @dataset.text_search campos['q']
			puts resultados
  		json_response resultados
  	elsif !campos.empty?
  		resultados = @dataset.where campos
      json_response resultados
  	end
  end

  def create
  	params.permit!
  	JSON.parse( params[@dataset_name.pluralize.to_sym] ).each do |entity|
  		@dataset.create entity
  	end
  end

  protected def dataset_name
    @dataset_name = request.headers['REQUEST_PATH'].split('/').last.singularize
    @dataset = "#{@dataset_name.capitalize}".constantize
  end
end
