module Restrictions
	extend ActiveSupport::Concern

	included do 
		before_action :authorized?, only: :create
	end

	def authorized?
		unless Admin.where(token: params[:token]).empty?
			true
		else
			render json: {status: '403 Forbidden', dados: { titulo: 'Usuário não autorizado ou token inválido' }}
		end
	end
end