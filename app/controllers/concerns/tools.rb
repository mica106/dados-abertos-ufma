module Tools
  extend ActiveSupport::Concern

  def json_response(data)
    render json: { data: data }
  end

  def ignored_params(params, ignored = %w(format controller action token))
    Hash[params.to_h.map{ |param| param unless (ignored.include? param.first) }.compact]
  end
end
