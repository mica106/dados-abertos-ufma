class Admin
	include Mongoid::Document
	include Mongoid::Timestamps

	before_create { token_generate() }
	validates :nome, presence: true

	field :nome, type: String
	field :token, type: String

	private def token_generate
		loop {
			self.token = SecureRandom.hex(10)
			break unless Admin.where(token: self.token).exists?
		}
	end
end
