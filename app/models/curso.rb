class Curso
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Attributes::Dynamic
  
  store_in collection: 'cursos'

  index({ 'coordenacao do programa': "text", 'nome': "text", 'descricao': "text"})
end
