class Departamento
  include Mongoid::Document
	include Mongoid::Timestamps

  store_in collection: 'departamentos'

  field :nome, type: String

  index({'nome': 'text'})
end
