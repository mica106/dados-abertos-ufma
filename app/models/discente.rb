class Discente
	include Mongoid::Document
	include Mongoid::Timestamps
  
  store_in collection: 'discentes'
	
	field :nome, type: String
	field :matricula, type: String
	field :curso_id, type: String

	validates :matricula, uniqueness: true
	
	def self.grab(object)
		object.map { |discente| Hash[Pessoal::Discente.fields.keys.map { |key| [key, discente[key]] if key != '_id'}.compact] }
	end
end
	