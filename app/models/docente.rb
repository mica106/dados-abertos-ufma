class Docente
  include Mongoid::Document
  include Mongoid::Timestamps
  
  store_in collection: 'docentes'

  field :siape, type: String
  field :nome, type: String
  field :descricao, type: String
  field :departamento, type: String, as: :subunidade
  field :departamento_codigo, type: String, as: :codigo_subunidade
  field :areas_de_interesse, type: String
  field :formacao_academica, type: String
  field :url_sigaa, type: String
  field :url_lattes, type: String

  validates :siape, uniqueness: true

  index({'formacao_academica': "text", 'areas_de_interesse': "text", 'descricao': "text", 'nome': "text"})

  def self.grab(object)
    object.map { |discente| Hash[Pessoal::Docente.fields.keys.map{ |key| [key, discente[key]] if key != '_id'}.compact] }
  end
end