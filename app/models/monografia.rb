class Monografia
  include Mongoid::Document
  
  store_in collection: 'monografias'
  
  field :ano, type: String
  field :data, type: String
  field :discente, type: String
  field :orientador, type: String
  field :titulo, type: String
  field :curso_id, type: String
  
  index({ 'titulo': 'text' })
  
  def self.grab object
    object.map { |monografia| Hash[Educacional::Monografia.fields.keys.map{ |key| [key, monografia[key]] if key != '_id'}.compact] }
  end
end
