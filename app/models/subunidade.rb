class Subunidade
  include Mongoid::Document
  include Mongoid::Timestamps
  
  store_in collection: 'subunidades'

  field :codigo, type: String
  field :nome, type: String
  field :localidade, type: String
  field :programa, type: String

  index({'nome': "text"})

  def self.grab(object)
    object.map { |subunidade| Hash[Institucional::Subunidade.fields.keys.map{ |key| [key, subunidade[key]] if key != '_id'}.compact] }
  end
end
