class Turma
  include Mongoid::Document
  include Mongoid::Timestamps
  
  store_in collection: 'turmas'

  field :nivel, type: String
  field :semestre, type: String
  field :codigo, type: String
  field :nome, type: String
  field :ch, type: String
  field :siape_docente, type: String
  field :tid, type: String

  index({'nome': 'text'})

  def self.grab(object)
    object.map { |turma| Hash[Educacional::Turma.fields.keys.map{ |key| [key, turma[key]] if key != '_id'}.compact] }
  end
end
