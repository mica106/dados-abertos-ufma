class Unidade
  include Mongoid::Document
  
  store_in collection: 'unidades'

  field :nome, type: String
  field :sigla, type: String
  field :url_sigaa, type: String
  field :apresentacao, type: String
  field :diretor, type: String
  field :siape_diretor, type: String

  index({ 'nome': "text", 'sigla': "text"})

  def self.grab(object)
  	object.map { |discente| Hash[Institucional::Unidade.fields.keys.map{ |key| [key, discente[key]] if key != '_id'}.compact] }
  end
end
