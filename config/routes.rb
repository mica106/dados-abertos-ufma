Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api, defaults: { format: :json } do
  	namespace :v01 do
  		get '/cursos' => 'datasets#all'
  		get '/discentes' => 'datasets#all'
  		get '/unidades' => 'datasets#all'
      get '/docentes' => 'datasets#all'
      get '/turmas' => 'datasets#all'
      get '/subunidades' => 'datasets#all'
      get '/monografias' => 'datasets#all'
      get '/departamentos' => 'datasets#all'

  		post '/cursos' => 'cursos#create'
  		post '/discentes' => 'discentes#create'
  		post '/unidades' => 'unidades#create'
      post '/docentes' => 'docentes#create'
      post '/turmas' => 'turmas#create'
      post '/subunidades' => 'subunidades#create'
      post '/monografias' => 'monografias#create'
      post '/departamentos' => 'datasets#create'

      get '/update' => 'datasets#update'
  	end
  end
  root 'inicio#index'
end
