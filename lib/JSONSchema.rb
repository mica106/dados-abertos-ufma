module JSONSchema

	def self.resposta(*attr)
		{
			status:	attr[:status],
			data:	attr[:data]
		}
	end
end